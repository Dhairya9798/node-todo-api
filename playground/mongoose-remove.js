const {ObjectId} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// Todo.remove({}).then((result) => {
//   console.log(result);
// });

//Todo.findOneAndRemove
//Todo.findByIdAndRemove

Todo.findOneAndRemove({_id: '5c7cfc96180f062f3c02f6a5'}).then((todo) => {
  console.log(todo);
});

Todo.findByIdAndRemove('5c7cfc96180f062f3c02f6a5').then((todo) => {
  console.log(todo);
});
